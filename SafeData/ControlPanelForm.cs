﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Text.RegularExpressions;


namespace SafeData
{

    public partial class ControlPanelForm : Form
    {
        //
        // ПАРАМЕТРЫ КЛАССА
        //

        // СВОЙСТВА БАЗЫ И ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ
        private String _currentLogin;
        private String _currentPassword;
        private bool _currentRestrain;
        private List<String> _logins;

        public bool DoAskToEnterPassword;

        // СВОЙСТВА xml-БАЗЫ
        public XmlDocument xDoc;
        public XmlElement xRoot;
        public string currentDir;

        //
        // ПУБЛИЧНЫЕ МЕТОДЫ СВОЙСТВ
        //

        public String currentLogin
        {
            get { return this._currentLogin;}
            set { this._currentLogin = value; }
        }

        public String currentPassword
        {
            get { return this._currentPassword; }
            set { this._currentPassword = value; }
        }

        public bool currentRestrain
        {
            get { return this._currentRestrain; }
            set { this._currentRestrain = value; }
        }

        public List<String> LoginsList
        {
            get { return this._logins; }
            set { this._logins = value; }
        }

        //
        // КОНСТРУКТОР ФОРМЫ (ТЕКУЩИЙ ЛОГИН, ТЕКУЩИЙ ПАРОЛЬ)
        //

        public ControlPanelForm(String CL, String CP, bool R)
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;

            // ИНИЦИАЛИЗАЦИЯ СВОЙСТВ

            LoginsList = new List<String>();
            currentLogin = CL;
            currentPassword = CP;
            currentRestrain = R;

            if (currentPassword == "")
            {
                DoAskToEnterPassword = false;
            }
            else
            {
                DoAskToEnterPassword = true;
            }

            // ПОЛУЧИТЬ СПИСОК ВСЕХ ИМЕЮЩИХСЯ В БАЗЕ ЛОГИНОВ

            currentDir = Environment.CurrentDirectory;
            //currentDir = currentDir.Substring(0, currentDir.IndexOf("bin"));
            xDoc = new XmlDocument();
            xDoc.Load(currentDir + "usersdata.xml");
            // получим корневой элемент
            xRoot = xDoc.DocumentElement;

            foreach (XmlNode xnode in xRoot)
            {
                if (xnode.Attributes.Count > 0)
                {
                    XmlNode attr = xnode.Attributes.GetNamedItem("login");
                    LoginsList.Add(attr.Value);
                }
            }

            // ЕСЛИ НЕ АДМИН - ЗАБЛОКИРОВАТЬ ВСЕ КНОПКИ, КРОМЕ СМЕНЫ ПАРОЛЯ
            if (currentLogin != "admin")
            {
                BTN_AddUser.Enabled = false;
                BTN_BlockUser.Enabled = false;
                BTN_Preferences.Enabled = false;
                BTN_ShowUsersDetails.Enabled = false;
            }

            // ЕСЛИ ЕСТЬ ОГРАНИЧЕНИЕ, А ПАРОЛЬ НЕ ПОДХОДИТ - ПРОСЬБА СМЕНИТЬ ПАРОЛЬ
            if (currentRestrain && !passwordSatisfiesRestrain(currentPassword))
            {
                MessageBox.Show("Ваш пароль не соответствует \nпоставленным на Ваш аккаунт ограничениям.\nПожалуйста, смените пароль.", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                DoAskToEnterPassword = false;
                ChangePassword();
            }
            
        }

        // ПРОВЕРКА ПАРОЛЯ НА ОГРАНИЧЕНИЕ
        bool passwordSatisfiesRestrain(String Password)
        {
            int errorCounter1 = Regex.Matches(Password, @"[a-zA-Z]").Count;
            int errorCounter2 = Regex.Matches(Password, @"[,.!?]").Count;
            if (errorCounter1 == 0 || errorCounter2 == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //
        // МЕТОДЫ ИНТЕРФЕЙСНОГО ВЗАИМОДЕЙСТВИЯ
        //

        // ВЫХОД ИЗ ПРИЛОЖЕНИЯ
        private void button6_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }

        // СМЕНА ПАРОЛЯ
        private void ChangePassword()
        {
            foreach (XmlNode xnode in xRoot)
            {
                // получаем атрибут name
                if (xnode.Attributes.Count > 0)
                {
                    XmlNode attr = xnode.Attributes.GetNamedItem("login");
                    if (attr.Value == currentLogin)
                    {
                        if (xnode.FirstChild != null && xnode.FirstChild.Name == "password" && xnode.FirstChild.InnerText == currentPassword)
                        {
                            PasswordCheckForm PCF = new PasswordCheckForm(currentPassword);
                            try
                            {
                                if (DoAskToEnterPassword)
                                {
                                    PCF.ShowDialog(); //проверка пароля
                                }
                                ChangePasswordForm CPF = new ChangePasswordForm(this);
                                CPF.password = currentPassword;
                                CPF.ShowDialog();
                                xnode.FirstChild.InnerText = currentPassword; //смена пароля в XML-базе
                            }
                            catch (Exception ex)
                            {
                                //если попалось исключение в PCF или CPF - вывести какое и вернуться в контрольную панель
                                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                        }
                    }
                }
            }
            //сохранить изменения в XML-базе
            xDoc.Save(currentDir + "usersdata.xml");
        }

        private void BTN_ChangeAdminPassword_Click(object sender, EventArgs e)
        {
            ChangePassword();            
        }

        // ПОКАЗАТЬ СПИСОК ПОЛЬЗОВАТЕЛЕЙ И ИХ ПАРАМЕТРЫ
        private void BTN_ShowUsersDetails_Click(object sender, EventArgs e)
        {
            PasswordCheckForm PCF = new PasswordCheckForm(currentPassword);
            try
            {
                //PCF.ShowDialog(); //проверка пароля

                UserListForm ULF = new UserListForm();
                ULF.Show();
            }
            catch (Exception ex)
            {
                //если попалось исключение в PCF или CPF - вывести какое и вернуться в контрольную панель
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }

        // ДОБАВИТЬ ПОЛЬЗОВАТЕЛЯ
        private void BTN_AddUser_Click(object sender, EventArgs e)
        {            

            // ДОБАВИТЬ НОВЫЙ ЛОГИН

            AddUser AUF = new AddUser(LoginsList);
            AUF.ShowDialog();
            String newLogin = "";

            if (LoginsList[1] == "NEW")
            {
                newLogin = LoginsList[0];
                LoginsList.RemoveAt(1);
            }

            XmlElement userElem = xDoc.CreateElement("user");
            // создаем атрибут name
            XmlAttribute nameAttr = xDoc.CreateAttribute("login");
            // создаем элементы company и age
            XmlElement passElem = xDoc.CreateElement("password");
            XmlElement blockElem = xDoc.CreateElement("block");
            XmlElement restrainElem = xDoc.CreateElement("restrain");
            // создаем текстовые значения для элементов и атрибута
            XmlText nameText = xDoc.CreateTextNode(newLogin);
            XmlText passText = xDoc.CreateTextNode("");
            XmlText blockText = xDoc.CreateTextNode("-");
            XmlText restrainText = xDoc.CreateTextNode("-");

            nameAttr.AppendChild(nameText);
            passElem.AppendChild(passText);
            blockElem.AppendChild(blockText);
            restrainElem.AppendChild(restrainText);

            userElem.Attributes.Append(nameAttr);
            userElem.AppendChild(passElem);
            userElem.AppendChild(blockElem);
            userElem.AppendChild(restrainElem);

            xRoot.AppendChild(userElem);

            //сохранить изменения в XML-базе
            xDoc.Save(currentDir + "usersdata.xml");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BTN_BlockUser_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AboutBox AB = new AboutBox();
            AB.Show();
        }

    }
}
