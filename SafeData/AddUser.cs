﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SafeData
{
    public partial class AddUser : Form
    {
        private List<String> logins
        { get; set; }

        public AddUser(List<String> L)
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;


            this.logins = L;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String newLogin = textBox1.Text;
            if (logins.Contains(newLogin))
            {
                MessageBox.Show("Такой логин уже существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                logins.Insert(0, "NEW");
                logins.Insert(0, newLogin);
                Close();
            }
        }
    }
}
