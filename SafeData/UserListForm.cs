﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace SafeData
{
    public partial class UserListForm : Form
    {
        public UserListForm()
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;

            this.InitDataGrid();
        
        
        }

        public void InitDataGrid()
        {
            // ИНИЦИАЛИЗАЦИЯ ЭЛЕМЕНТОВ ТАБЛИЦЫ

            DataGridViewCheckBoxColumn BLOCKcolumn = new DataGridViewCheckBoxColumn();
            {
                BLOCKcolumn.HeaderText = "BLOCK";
                BLOCKcolumn.Name = "BLOCK";
                BLOCKcolumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                BLOCKcolumn.FlatStyle = FlatStyle.Standard;
                BLOCKcolumn.ThreeState = false;
                BLOCKcolumn.CellTemplate = new DataGridViewCheckBoxCell();
                BLOCKcolumn.CellTemplate.Style.BackColor = Color.Pink;
            }
            DataGridViewCheckBoxColumn RESTRAINcolumn = new DataGridViewCheckBoxColumn();
            {
                RESTRAINcolumn.HeaderText = "RESTRAIN";
                RESTRAINcolumn.Name = "RESTRAIN";
                RESTRAINcolumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                RESTRAINcolumn.FlatStyle = FlatStyle.Standard;
                RESTRAINcolumn.ThreeState = false;
                RESTRAINcolumn.CellTemplate = new DataGridViewCheckBoxCell();
                RESTRAINcolumn.CellTemplate.Style.BackColor = Color.Beige;
            }


            dataGridView1.DefaultCellStyle.NullValue = false;
            dataGridView1.ColumnCount = 2;
            dataGridView1.Columns[0].Name = "LOGIN";
            dataGridView1.Columns[1].Name = "PASSWORD";
            dataGridView1.Columns.Insert(2, BLOCKcolumn);
            dataGridView1.Columns.Insert(3, RESTRAINcolumn);

            // ИНИЦИАЛИЗАЦИЯ XML ФАЙЛА

            string currentDir = Environment.CurrentDirectory;
            //currentDir = currentDir.Substring(0, currentDir.IndexOf("bin"));
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(currentDir + "usersdata.xml");
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            // ЗАПОЛНЕНИЕ ТАБЛИЦЫ ДАННЫМИ ИЗ XML ФАЙЛА

            foreach (XmlNode xnode in xRoot)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);

                if (xnode.Attributes.Count > 0)
                {
                    XmlNode attr = xnode.Attributes.GetNamedItem("login");
                    row.Cells[0].Value = attr.Value;

                    foreach (XmlNode childNode in xnode.ChildNodes)
                    {
                        if (childNode.Name == "password")
                        {
                            row.Cells[1].Value = xnode.ChildNodes[0].InnerText;
                        }
                        else if (childNode.Name == "block")
                        {
                            DataGridViewCheckBoxCell CBC = new DataGridViewCheckBoxCell(false);
                            if (xnode.ChildNodes[1].InnerText == "+")
                            {
                                CBC.Value = true;
                                row.Cells[2].Value = true;
                            }
                            else
                            {
                                CBC.Value = false;
                                row.Cells[2].Value = false;
                            }
                        }
                        else if (childNode.Name == "restrain")
                        {
                            DataGridViewCheckBoxCell CBC = new DataGridViewCheckBoxCell(false);
                            if (xnode.ChildNodes[2].InnerText == "+")
                            {
                                CBC.Value = true;
                                row.Cells[3].Value = true;
                            }
                            else
                            {
                                CBC.Value = false;
                                row.Cells[3].Value = false;
                            }
                        }
                    }

                }

                dataGridView1.Rows.Add(row);
            }

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataGridView1.AllowUserToAddRows = false;
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 2) //block
            {
                // ИНИЦИАЛИЗАЦИЯ XML ФАЙЛА

                string currentDir = Environment.CurrentDirectory;
                //currentDir = currentDir.Substring(0, currentDir.IndexOf("bin"));
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(currentDir + "usersdata.xml");
                // получим корневой элемент
                XmlElement xRoot = xDoc.DocumentElement;

                // РЕДАКТИРОВАНИЕ ДАННЫХ

                foreach (XmlNode xnode in xRoot)
                {
                    if (xnode.Attributes.Count > 0)
                    {
                        XmlNode attr = xnode.Attributes.GetNamedItem("login");
                        String login = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                        if (attr.Value == login)
                        {
                            foreach (XmlNode childNode in xnode.ChildNodes)
                            {
                                if (childNode.Name == "block")
                                {
                                    if ((bool)dataGridView1.Rows[e.RowIndex].Cells[2].Value == true)
                                    {
                                        childNode.InnerText = "-";
                                    }
                                    else
                                    {
                                        childNode.InnerText = "+";
                                    }
                                }
                            }
                        }

                    }
                }
                xDoc.Save(currentDir + "usersdata.xml");
            }
            else if (e.ColumnIndex == 3) //restrain
            {
                // ИНИЦИАЛИЗАЦИЯ XML ФАЙЛА

                string currentDir = Environment.CurrentDirectory;
                //currentDir = currentDir.Substring(0, currentDir.IndexOf("bin"));
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(currentDir + "usersdata.xml");
                // получим корневой элемент
                XmlElement xRoot = xDoc.DocumentElement;

                // РЕДАКТИРОВАНИЕ ДАННЫХ

                foreach (XmlNode xnode in xRoot)
                {
                    if (xnode.Attributes.Count > 0)
                    {
                        XmlNode attr = xnode.Attributes.GetNamedItem("login");
                        String login = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                        if (attr.Value == login)
                        {
                            foreach (XmlNode childNode in xnode.ChildNodes)
                            {
                                if (childNode.Name == "restrain")
                                {
                                    if ((bool)dataGridView1.Rows[e.RowIndex].Cells[3].Value == true)
                                    {
                                        childNode.InnerText = "-";
                                    }
                                    else
                                    {
                                        childNode.InnerText = "+";
                                    }
                                }
                            }
                        }

                    }
                }
                xDoc.Save(currentDir + "usersdata.xml");
            }
        }
    

    }

}
