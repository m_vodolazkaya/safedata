﻿namespace SafeData
{
    partial class ControlPanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanelForm));
            this.button6 = new System.Windows.Forms.Button();
            this.BTN_Preferences = new System.Windows.Forms.Button();
            this.BTN_BlockUser = new System.Windows.Forms.Button();
            this.BTN_AddUser = new System.Windows.Forms.Button();
            this.BTN_ShowUsersDetails = new System.Windows.Forms.Button();
            this.BTN_ChangeAdminPassword = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button6.Location = new System.Drawing.Point(11, 262);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(274, 28);
            this.button6.TabIndex = 5;
            this.button6.Text = "Выход из приложения";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // BTN_Preferences
            // 
            this.BTN_Preferences.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BTN_Preferences.Location = new System.Drawing.Point(11, 148);
            this.BTN_Preferences.Name = "BTN_Preferences";
            this.BTN_Preferences.Size = new System.Drawing.Size(274, 28);
            this.BTN_Preferences.TabIndex = 6;
            this.BTN_Preferences.Text = "Настройка ограничений на пароли";
            this.BTN_Preferences.UseVisualStyleBackColor = true;
            this.BTN_Preferences.Visible = false;
            // 
            // BTN_BlockUser
            // 
            this.BTN_BlockUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BTN_BlockUser.Location = new System.Drawing.Point(11, 114);
            this.BTN_BlockUser.Name = "BTN_BlockUser";
            this.BTN_BlockUser.Size = new System.Drawing.Size(274, 28);
            this.BTN_BlockUser.TabIndex = 7;
            this.BTN_BlockUser.Text = "Блокировка пользователя";
            this.BTN_BlockUser.UseVisualStyleBackColor = true;
            this.BTN_BlockUser.Visible = false;
            this.BTN_BlockUser.Click += new System.EventHandler(this.BTN_BlockUser_Click);
            // 
            // BTN_AddUser
            // 
            this.BTN_AddUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BTN_AddUser.Location = new System.Drawing.Point(12, 80);
            this.BTN_AddUser.Name = "BTN_AddUser";
            this.BTN_AddUser.Size = new System.Drawing.Size(273, 28);
            this.BTN_AddUser.TabIndex = 8;
            this.BTN_AddUser.Text = "Добавить пользователя";
            this.BTN_AddUser.UseVisualStyleBackColor = true;
            this.BTN_AddUser.Click += new System.EventHandler(this.BTN_AddUser_Click);
            // 
            // BTN_ShowUsersDetails
            // 
            this.BTN_ShowUsersDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BTN_ShowUsersDetails.Location = new System.Drawing.Point(12, 46);
            this.BTN_ShowUsersDetails.Name = "BTN_ShowUsersDetails";
            this.BTN_ShowUsersDetails.Size = new System.Drawing.Size(273, 28);
            this.BTN_ShowUsersDetails.TabIndex = 9;
            this.BTN_ShowUsersDetails.Text = "Просмотр параметров пользователей";
            this.BTN_ShowUsersDetails.UseVisualStyleBackColor = true;
            this.BTN_ShowUsersDetails.Click += new System.EventHandler(this.BTN_ShowUsersDetails_Click);
            // 
            // BTN_ChangeAdminPassword
            // 
            this.BTN_ChangeAdminPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BTN_ChangeAdminPassword.Location = new System.Drawing.Point(12, 12);
            this.BTN_ChangeAdminPassword.Name = "BTN_ChangeAdminPassword";
            this.BTN_ChangeAdminPassword.Size = new System.Drawing.Size(273, 28);
            this.BTN_ChangeAdminPassword.TabIndex = 10;
            this.BTN_ChangeAdminPassword.Text = "Сменить пароль";
            this.BTN_ChangeAdminPassword.UseVisualStyleBackColor = true;
            this.BTN_ChangeAdminPassword.Click += new System.EventHandler(this.BTN_ChangeAdminPassword_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(12, 228);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(274, 28);
            this.button1.TabIndex = 11;
            this.button1.Text = "Выход из системы";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(11, 194);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(274, 28);
            this.button2.TabIndex = 12;
            this.button2.Text = "О системе";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ControlPanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 302);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BTN_ChangeAdminPassword);
            this.Controls.Add(this.BTN_ShowUsersDetails);
            this.Controls.Add(this.BTN_AddUser);
            this.Controls.Add(this.BTN_BlockUser);
            this.Controls.Add(this.BTN_Preferences);
            this.Controls.Add(this.button6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ControlPanelForm";
            this.Text = "Контрольная панель";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.controlPanelForm_FormClosing);
            this.ResumeLayout(false);

        }

        private void controlPanelForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            //throw new System.NotImplementedException();
            BTN_AddUser.Enabled = true;
            BTN_BlockUser.Enabled = true;
            BTN_Preferences.Enabled = true;
            BTN_ShowUsersDetails.Enabled = true;
        }

        #endregion

        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button BTN_Preferences;
        private System.Windows.Forms.Button BTN_BlockUser;
        private System.Windows.Forms.Button BTN_AddUser;
        private System.Windows.Forms.Button BTN_ShowUsersDetails;
        private System.Windows.Forms.Button BTN_ChangeAdminPassword;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}