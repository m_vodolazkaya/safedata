﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeData
{
    class User
    {
        private String _login;
        private String _password;
        private bool _block;
        private bool _restrain;
        private int _passwordLength;

        public String login
        {
            get { return _login; }
            set { _login = value; }
        }

        public String password
        {
            get { return _password; }
            set { _password = value; }
        }

        public bool block
        {
            get { return _block; }
            set { _block = value; }
        }

        public bool restrain
        {
            get { return _restrain; }
            set { _restrain = value; }
        }

        public int passwordLength
        {
            get { return 0; } // _password.Length; }
        }
    }
}
