﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SafeData
{
    public partial class PasswordCheckForm : Form
    {
        private String _RightPassword
        { get; set; }

        public PasswordCheckForm(String RightPassword)
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;


            _RightPassword = RightPassword;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == _RightPassword)
            {
                Close();
            }
            else
            {
                throw new Exception("Введён неверный пароль");
            }
        }

        private void PasswordCheckForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1.PerformClick();
            }
        }

        private void PasswordCheckForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //throw new Exception("Пароль не был введён");
        }
    }
}
