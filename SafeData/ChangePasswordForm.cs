﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Xml;

namespace SafeData
{
    public partial class ChangePasswordForm : Form
    {
        public String password
        { get; set; }
        private ControlPanelForm controlPanel
        { get; set; }

        private bool CHANGE_MODE;
        public String NewPassword;
        public bool RESTRAIN = false;

        public ChangePasswordForm(ControlPanelForm CP)
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;

            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;

            button1.Enabled = false;

            RESTRAIN = CP.currentRestrain;

            if (!RESTRAIN)
            {
                label3.ForeColor = Color.DarkGray;
                label4.ForeColor = Color.DarkGray;
                label5.ForeColor = Color.DarkGray;
            }

            this.controlPanel = CP;
            
            this.CHANGE_MODE = true;
            label1.Text = "Введите новый пароль:";
            label2.Text = "Повторите пароль:";
        }

        public ChangePasswordForm(String pass)
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;

            this.NewPassword = pass;
            this.CHANGE_MODE = false;
            label1.Text = "Выберите себе пароль:";
            label2.Text = "Повторите пароль:";

            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            label3.ForeColor = Color.DarkGray;
            label4.ForeColor = Color.DarkGray;
            label5.ForeColor = Color.DarkGray;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.CHANGE_MODE)
            {
                if (textBox1.Text == textBox2.Text)
                {
                    if (password == textBox1.Text)
                    {
                        MessageBox.Show("Этот пароль совпадает с предыдущим, \nвыберите другой пароль.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        this.controlPanel.currentPassword = textBox1.Text;
                        Close();
                    }
                }
            }
            else
            {
                if (textBox1.Text == textBox2.Text)
                {
                    this.NewPassword = textBox1.Text;
                    Close();
                }
            }
        }

        // ПРОВЕРКА ПАРОЛЯ НА ОГРАНИЧЕНИЕ
        bool passwordSatisfiesRestrain(String Password)
        {
            int errorCounter1 = Regex.Matches(Password, @"[a-zA-Z]").Count;
            int errorCounter2 = Regex.Matches(Password, @"[,.!?]").Count;
            if (errorCounter1 == 0 || errorCounter2 == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void ChangePasswordForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1.PerformClick();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            String newPassword = textBox1.Text;

            if (RESTRAIN)
            {
                int errorCounter1 = Regex.Matches(newPassword, @"[a-zA-Z]").Count;
                if (errorCounter1 != 0)
                {
                    pictureBox1.Visible = true;
                }
                else
                {
                    pictureBox1.Visible = false;
                }

                int errorCounter2 = Regex.Matches(newPassword, @"[,.!?]").Count;
                if (errorCounter2 != 0)
                {
                    pictureBox2.Visible = true;
                }
                else
                {
                    pictureBox2.Visible = false;
                }

                if (passwordSatisfiesRestrain(newPassword))
                {
                    pictureBox3.Visible = true;
                }
                else
                {
                    pictureBox3.Visible = false;
                }
            }
            else
            {
                pictureBox3.Visible = true;
            }

            if (textBox1.Text == textBox2.Text)
            {
                pictureBox4.Visible = true;
            }
            else
            {
                pictureBox4.Visible = false;
            }

            if (pictureBox3.Visible && pictureBox4.Visible)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == textBox2.Text)
            {
                pictureBox4.Visible = true;
            }
            else
            {
                pictureBox4.Visible = false;
            }

            if (pictureBox3.Visible && pictureBox4.Visible)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }
    }
}
