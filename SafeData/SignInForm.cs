﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Xml;

namespace SafeData
{
    public partial class SignInForm : Form
    {
        private int WrongPassword;
        private bool loginIsInTheBase;

        /* HOTKEYS SETUP
        // DLL libraries used to manage hotkeys
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        const int MYACTION_HOTKEY_ID = 1;
        const int MYACTION_HOTKEY_ID2 = 2;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312 && m.WParam.ToInt32() == MYACTION_HOTKEY_ID)
            {
                // My hotkey has been typed
                button1.PerformClick();
            }

            base.WndProc(ref m);
        }
        */


        public SignInForm()
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;


            WrongPassword = 1;
            loginIsInTheBase = false;

            createFile();

            // Modifier keys codes: Alt = 1, Ctrl = 2, Shift = 4, Win = 8
            // Compute the addition of each combination of the keys you want to be pressed
            // ALT+CTRL = 1 + 2 = 3 , CTRL+SHIFT = 2 + 4 = 6...
            //RegisterHotKey(this.Handle, MYACTION_HOTKEY_ID, 0, (int)Keys.Enter);

        }

        private void createFile()
        {
            string currentDir = Environment.CurrentDirectory;
            //currentDir = currentDir.Substring(0, currentDir.IndexOf("bin"));
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(currentDir + "usersdata.xml");
            }
            catch (Exception)
            {
                XmlTextWriter writer = new XmlTextWriter(currentDir + "usersdata.xml", System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("users");
                createNode("NO-DATA", "-", "-", writer);
                writer.WriteEndElement();
                
                writer.WriteEndDocument();
                writer.Close();
                
                MessageBox.Show("XML файл базы пользователей создан. \nЛогин админа: admin. Пароль - пустой.\nПри попытке зайти с пустым паролем \nВам будет предложено выбрать пароль.", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void createNode(string password, string block, string restrain, XmlTextWriter writer)
        {
            writer.WriteStartElement("user");
            writer.WriteAttributeString("login", "admin");
            
            writer.WriteStartElement("password");
            writer.WriteString(password);
            writer.WriteEndElement();

            writer.WriteStartElement("block");
            writer.WriteString(block);
            writer.WriteEndElement();
            
            writer.WriteStartElement("restrain");
            writer.WriteString(restrain);
            writer.WriteEndElement();
            
            writer.WriteEndElement();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String login = "";
            String password = "";

            try
            {
                if (loginTB.Text == "")
                {
                    MessageBox.Show("Не введено имя пользователя", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                login = loginTB.Text;
                password = passwordTB.Text;

                string currentDir = Environment.CurrentDirectory;
                //currentDir = currentDir.Substring(0, currentDir.IndexOf("bin"));
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(currentDir + "usersdata.xml");
                // получим корневой элемент
                XmlElement xRoot = xDoc.DocumentElement;

                foreach (XmlNode xnode in xRoot)
                {
                    // получаем атрибут name
                    if (xnode.Attributes.Count > 0)
                    {
                        XmlNode attr = xnode.Attributes.GetNamedItem("login");
                        if (attr.Value == login)
                        {
                            loginIsInTheBase = true;
                            if (login == "admin" && xnode.FirstChild.InnerText == "NO-DATA")
                            {
                                ChangePasswordForm CPF = new ChangePasswordForm(password);
                                CPF.ShowDialog();
                                if (CPF.NewPassword != "")
                                {
                                    xnode.FirstChild.InnerText = CPF.NewPassword;
                                    xDoc.Save(currentDir + "usersdata.xml");
                                }
                            } 
                            else if (xnode.FirstChild != null && xnode.FirstChild.Name == "password" && xnode.FirstChild.InnerText == password)
                            {
                                if (xnode.ChildNodes[1].Name == "block" && xnode.ChildNodes[1].InnerText == "+")
                                {
                                    MessageBox.Show("Этот пользователь заблокирован.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    this.passwordTB.Text = "";
                                    this.loginTB.Text = "";
                                }
                                else
                                {
                                    bool restrain = (xnode.LastChild.InnerText == "+");
                                    ControlPanelForm controlPanelForm = new ControlPanelForm(login, password, restrain);
                                    controlPanelForm.Show();
                                    this.Hide();
                                    controlPanelForm.FormClosing += controlPanelForm_FormClosing;
                                }
                            }
                            else
                            {
                                switch (WrongPassword)
                                {
                                    case 1:
                                        {
                                            WrongPassword++;
                                            MessageBox.Show("Неверный пароль!\nПри трёхкратном введении неверного пароля \nприложение завершится.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        }
                                        break;
                                    case 2:
                                        {
                                            WrongPassword++;
                                            MessageBox.Show("Неверный пароль!\nПри трёхкратном введении неверного пароля \nприложение завершится.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        }
                                        break;
                                    case 3:
                                        {
                                            MessageBox.Show("Пароль был неверно введён три раза подряд.\nПриложение будет завершено.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            Application.Exit();
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
                if (!loginIsInTheBase)
                {
                    throw new Exception("Такого логина нет в базе.\nПопробовать снова?");
                }
            }
            catch (Exception ex)
            {
                if ((MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question)) != DialogResult.Retry)
                {
                    Application.Exit();
                }
            }

        }

        void controlPanelForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.loginTB.Text = "";
            this.passwordTB.Text = "";
            this.Show();
        }

        private void SignInForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void SignInForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1.PerformClick();
            }
        }

        private void SignInForm_Load(object sender, EventArgs e)
        {

        }

       
    }
}
